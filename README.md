# springboot-hello

#### 介绍
springboot的helloworld项目，一般用于打jar包到服务器测试是否正常。默认8080端口，只有一个接口：http://localhost:8080/hello

#### 软件架构
软件架构说明


#### 安装教程

1.  git clone https://gitee.com/liangxianning/springboot-hello.git
2.  cd springboot-hello
3.  mvn clean install -Dmaven.test.skip=true

#### 使用说明

1.  nohup java -Xms128m -Xmx256m -jar springboot-hello-1.0.jar --server.port=18080 > springboot-hello.log 2>&1 &
2.  tail -f springboot-hello.log
3.  访问http://localhost:18080/hello
4.  停止：
    ps -ef | grep springboot,  kill -9 pid号

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
